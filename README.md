# ACME - Cinema Operator Tools - Table of contents

1. [Introduction](#introduction)
2. [Software Requirement Analysis](#requirement_analysis)
3. [Solution Implement & Data Flow Diagram](#solution)
4. [How to run](#how_to_run)
5. [Report & Review](#report_review)
6. [Demo](#demo)
7. [ToDo](#todo)

## ACME - Cinema Operator Tools <a name="introduction"></a>
ACME Co  - Cinema Operator Tools is a service to ensure that cinema goers respect social distancing. ACME can adopt to 
run its cinema operation.

## Software Requirement Analysis <a name="requirement_analysis"></a>
> Requirement: ./requirement/SWE_Coding Task [HCM_Hiring Test Teko_VNLife].pdf

### 1. Task 1: Support config flexible
> The service should be configurable to support a given cinema size and minimum distance (described below), which can 
be specified as: (rows, columns, min_distance).
- **Requirement**: Cần hỗ trợ config động được các thông số **rows**, **columns** và **min_distance** của rạp. 
- **Input**: 
    + **rows** : số row của matrix
    + **columns** : số column của matrix
    + **min_distance** : khoảng cách tối thiểu giữa 2 người được phép pick chỗ nếu thỏa. 

- **Output**: Cần đọc được config này và lưu trữ, sử dụng để xử lý bài toán. Có thể tạm thời, config sẽ tĩnh khi init service.
Chưa cần có sự thay đổi linh hoạt. 

### 2. Task 2: Support search a set of seats available
> The service should allow one RPC that cinema operations can use to query for a set of seats currently available for
 purchase. The input will be a number that specifies how many seats are needed. The seats should be together.
- **Requirement**: Yêu cầu tìm ra **tất cả** các set chỗ ngồi thỏa điều kiện `social distancing` với số lượng chỗ ngồi được yêu cầu,
và các chỗ ngồi này **cần phải gần nhau**. 
- **Input** : 
    + **a set of seats** : 1 số nguyên, thể hiện số chỗ ngồi mà User muốn tìm kiếm để đặt. 

- **Output**: 
    + Xuất ra được tất cả các set chỗ ngồi (array pair (x,y) với x, y là tọa độ của seat) mà thỏa điều kiện `social distancing`. 
    + Ngược lại, nếu không tồn lại chỗ ngồi hợp lệ thì trả về lỗi. 

- **Example**:
    + Dưới đây là ví dụ về yêu cầu trên: với các tham số: 
        + rows: **4**
        + columns: **5**
        + min_distance: **1**
        + set of seats: **2**
        
| Cinema Example | Result |
|:---|:---:|
| ![example](./docs/images/example_task2_requirement.png) |Có 4 set phù hợp với yêu cầu: {(1,0),(2,0)}<br>{(2,0),(2,1)}<br>{(2,0),(3,0)}<br>{(2,4),(3,4)}|

### Task 3: Support reserve a given set of seats
> The service should allow one RPC that operations can use to reserve a given set of seats, where seats are specified 
by a (row, column) coordinate ((0, 0) is top left).

- **Requirement**: Yêu cầu cần đặt chỗ của rạp với 1 dãy các ghế mong muốn (đã chọn sẵn tọa độ ghế), đồng thời cần phải
kiểm tra thỏa mãn điều kiện `social distancing` và trả về kết quả được phép đặt chỗ hay không?
- **Input**: 
    + 1 array các pair (x,y) trong đó (x,y) là tọa độ của 1 seat. 
- **Output**:
    + Kiểm tra tính hợp lệ của đặt chỗ? Nếu ok, cho phép đặt và lưu lại các ghế đã được đặt. Và trả về kết quả thành công.
    + Nếu không tồn tại tính hợp lệ của bộ seat, thì trả về lỗi. Thông báo không thể đặt chỗ. 
    
## Solution Implement & Data Flow Diagram <a name="solution"></a>

#### Task 1. The service should be configurable to support a given cinema size and minimum distance (described below), which can be specified as: (rows, columns, min_distance).

- Sẽ thực hiện read config (3 tham số **rows**, **columns**, **min_distance**) khi init service: 
    + Config sẽ được đọc theo thứ tự ưu tiên: `environment` > `file config` > `default config`
        + Default Config (_vì không có yêu cầu từ đề bài, nên sẽ tạm thời init với thông số theo ví dụ_): 
            ```json
            {
                "rows":5,
                "columns":4,
                "min_distance":7
            }
            ```
    + Nếu không tồn tại config hoặc config không valid -> Thực hiện panic service. Vì không thỏa mãn yêu cầu và đáp ứng được
    nhu cầu biz. 

    + Các điều kiện cần kiểm tra: 
        + `rows, columns > 0` -> Vì cần tồn tại số ghế lớn hơn 1 trong rạp. 
        + `min_distance >= 0` -> Vì `min_distance` có thể bằng 0 với điều kiện bình thường, có thể không theo quy định 
        `social distancing`
        + `min_distance <= rows + columns - 2` -> Vì maximum value của 1 rạp có thể thỏa mãn là `|rows - 1| + |columns - 1|`

- Next step (optimize) (_not implement yet_): 
    + Thực hiện đọc từ **Consul/ETCD** để thực hiện watch config khi có sự thay đổi

#### Task 2. The service should allow one RPC that cinema operations can use to query for a set of seats currently available for purchase. The input will be a number that specifies how many seats are needed. The seats should be together.

- Cần implement 1 method (`ex: QuerySeatsAvailable`) của `CinemaOperatorHandler` để serve được yêu cầu này. Với input và 
output của method đã được định nghĩa và phân tích trong mục trên. 

- Kiểm tra các giá trị input đầu vào: 
    + Kiểm tra `size of seats` yêu cầu phải lớn hơn 0, 
    + Kiểm tra `size of seats` phải nhỏ hơn số lượng ghế còn trống trong rạp. 
    
- Solution search `set of seat`:
    + Thực hiện implement 1 package `Search Engine`, phục vụ cho nhu cầu nhận vào 1 matrix (_rows x columns_), với giá trị
    của mỗi phần tử trong matrix bao gồm: 
        + **1** : Seat đã được đặt
        + **0** : Seat đang trống, có thể đặt chỗ. 
        + **-1** : Seat đã bị khóa không thể đặt, vì tuân thủ nguyên tắc `social distancing` 1 khoảng `min_distance`
    + Package `Search Engine` cần implement 1 method sẽ nhận vào các input trên, và `size_of_seat` cần tìm. Và thực hiện 
    thuật toán để tìm kiếm ra những set phù hợp. 
    ```go
      func SearchSetOfSeats(n, rows, columns int, cinemaMatrix [][]int) [][]models.Seat
    ```
    + Thuật toán tìm kiếm: 
        ```go
          func (s *searchEngine) findSetOfSeat(n int, result []models.Seat)
        ```
        Thực hiện thuật toán tìm kiếm đệ quy (_quy hoạch động_) quét lần lượt từng phần tử của mảng (trái -> phải, trên -> dưới). 
        Nếu trường hợp phần tử đó thỏa điều kiện:
         - Đang `available` (0)
         - Không nằm trong kết quả đã được tìm được (_result_)
         - Nằm **bên phải** hoặc **bên dưới** 1 phần tử bất kì trong danh sách kết quả tìm được (_result_)
         
        -> Thì thêm phần tử đó vào kết quả tìm được (_result_). 
        
        Sau đó, quay lại gọi tiếp tục gọi hàm `findSetOfSeat` để tìm kiếm tiếp những phần tử tiếp theo với số ghế cần tìm giảm đi 1 _(n - 1)_.
        Thuật toán dừng lại khi `n = 0`, đồng nghĩa đã tìm được set thỏa mãn yêu cầu đề toán. Ngược lại, nếu vị trí đang xét
        không thỏa mãn thì tiếp tục cho đến khi hết bảng ma trận chỗ ngồi. 
        
- Output của method `QuerySeatsAvailable`: 
    + Trả về tất cả các kết quả hợp lệ mà thuật toán tìm kiếm được. Nếu trường hợp tìm ra quá nhiều kết quả, thì sẽ chặn
    limit trả về `top 50 kết quả` tìm được. Vì tránh trường hợp trả quá nhiều kết quả (không phù hợp với nhau cầu thực tế,
    hoặc có thể paging, ...) đồng thời sẽ giảm thiểu tốn kém về memory. 
        + Giá trị `50` này được config động ở file `config`, để linh động thay đổi khi cần thiết.
    + Trường hợp không tìm thấy kết quả nào hợp lệ, trả về lỗi cho caller thực hiện handle biz logic và xử lý (mã lỗi định
    nghĩa bên dưới). 
    
- **Error Table**: 

| Error Message | Meaning |
|:---|:---:|
|NOT_ENOUGH_SEAT| Không đủ số ghế trống với số lượng chỗ yêu cầu |
|RESULT_IS_EMPTY| Không tồn tại set thỏa mãn với số chỗ yêu cầu |
|INVALID_PARAM_SIZE_OF_SET| Size of set không thỏa mãn (<0, = 0, ...)

#### Task 3. The service should allow one RPC that operations can use to reserve a given set of seats, where seats are specified by a (row, column) coordinate ((0, 0) is top left).

- Cần implement 1 method (`ex: ReserveSetOfSeats`) của `CinemaOperatorHandler` để serve được yêu cầu này. Với input và 
output của method đã được định nghĩa và phân tích trong mục trên. 
- Solution: 
    + **Step 1**: Thực hiện kiểm tra tính hợp lệ của các yêu cầu: 
        + Tính hợp lệ của array các `Seat` đã input. 
            + Size array of seat có lớn hơn tổng số của rạp có thể chứa được hay không (`rows * columns`)
            + Size array of seat có lớn hơn số ghế đang available hay không (`rows * columns - sum(seat(x,y)==1)`) (với
            value `1` is mark which seat at location with row `x` and `column `y` was picked)
    + Ở **Step 2** này có 2 hướng xử lý. Tuy nhiên, em trình bày 2 solution dưới đây, và em sẽ chọn solution 2 để implement. 
        + **Solution 1**: Thực hiện kiểm tra array of `Seat` input vào có là 1 trong những set tìm kiếm được với số lượng ghế
        bằng `len([]Seat)` thỏa điều kiện `social distancing` dựa trên `Search Engine` đã implement phía trên hay không?
             + Nếu không thỏa mãn, thực hiện return về lỗi cho caller. 
             + Nếu thỏa mãn yêu cầu, thực hiện đánh dấu vị trí các `Seat` trong array yêu cầu đã được đặt chỗ (`value` = 1) và
             trả về kết quả đặt chỗ thành công. 
        -> Solution này sẽ tận dụng lại kết quả câu trên, tuy nhiên việc truy vấn query hết tất cả các sets hợp lệ, và tìm
        1 set nằm trong tập kết quả trả ra sẽ tốn nhiều performance và latencies hơn. 
        
        + **Solution 2** Thực hiện implement 1 method trong package `Search Engine` để tìm kiếm các chỗ trong input yêu cầu
        (`request []Seat`) có thỏa mãn yêu cầu `social distancing` hay không (_các ghế phải cạnh nhau và đang available_)
            ```go 
                // Func hỗ trợ cho caller kiểm tra check `setOfSeats` có hợp lệ theo yêu cầu `social_distancing` hay không
                func CheckSetIsAvailable(rows, columns int, cinemaMatrix [][]int, setOfSeats []models.Seat) bool
                // Func con đệ quy để thực hiện kiểm tra danh sách trên có hợp lệ hay không?
                func (s *searchEngine) searchSetIsAvailable(input []models.Seat, result []models.Seat)
            ```
            Func này thực hiện nhận vào các input `rows, columns, cinemaMatrix` thông tin của chỗ ngồi (tương tự mục 2 ở trên)
            và danh sách các chỗ ngồi cần kiểm tra (`setOfSeats []models.Seat`). 
            + Thực hiện quét lần lượt từng phần tử trong `setOfSeats`, kiểm tra các yêu cầu: 
                + Ghế đang available
                + Ghế không nằm trong danh sách các ghế đã kiểm tra rồi (`result []models.Seat`)
                + Ghế có nằm bên cạnh (trên, dưới, trái, phải) bất kì phần tử nào đã mảng đã kiểm tra rồi (`result []models.Seat`)
                
            -> Nếu **thỏa mãn,** append ghế đó vào (`result []models.Seat`), xóa ghế đó đi khỏi danh sách cần kiểm tra (`setOfSeats`) 
            và tiếp tục đệ quy để tìm kiếm những ghế còn lại. Ngược lại nếu không, thì trả về ngay kết quả `set of seat` đó
            không hợp lệ. 
            + Nếu `len(setOfSeats)=0`, thì dừng vòng lặp đệ quy, và trả kết quả về. 
                
- Output: Trả về kết quả tìm kiếm được (true/false). Nếu tìm kiếm hợp lệ thì trả về kết quả `Thành công`. Ngược lại sẽ trả
về lỗi cho caller thực hiện handle logic biz. 

- Error Table: 

| Error Message | Meaning |
|:---|:---:|
|NOT_ENOUGH_SEAT| Không đủ số ghế trống với số lượng chỗ yêu cầu |
|REQ_SET_OF_SEAT_NOT_AVAILABLE| Danh sách ghế yêu cầu không thỏa mãn |

## TODO <a name="todo"></a>
- [x] Analyst Requirement
- [x] Find solution, and describe by text
- [x] Init service, and define file proto with 2 RPC
    - [x] define file proto and generate file proto-go
    - [x] init service with other package such as log, tracing,...
    - [x] init config & implement task 1 - load configuration package
- [x] Implement package `Search engine` to handle 2 requirement `Search all set of Seats` and `Reserve a given set of seats`
- [x] Unit test func in package `Search engine`
- [x] Implement method in main Handler (`CinemaOperator`) with 2 method `SearchSeats` and `ReserveSeats` seats. 
- [x] Unit test/mock test func in above handler. 
- [ ] Implement caching result set of seats on redis (_optimize_) (_In Progress_)
- [x] Deploy service on cloud to demo (_if necessary_)

## How to run <a name="how_to_run"></a>
- Point at root project, run comment `go run main.go` to compile and run service. 
- Or other way, you can test by test RPC on service demo, detail at [Demo](#demo)

## Report & Review <a name="report_review"></a>
### Check list requiment business: 
- [x] **Hoàn thành 100% yêu cầu**. Đã thực hiện phân tích yêu cầu, viết document hướng giải quyết và develop, implement 
các yêu cầu của đề bài đặt ra. 
- [x] **Test coverage 100%**. Thực hiện unit test và mock test các package logic chính (**_handler, operator, search engine_**), cụ thể sẽ trình bày
ở mục bên dưới. 

### Unit test/mock test:
- Thực hiện unit test và mock test (sử dụng `library gomock`) các logic chính và business logic với **coverage 100%**. 
    + ![Test coverage](./docs/images/test_coverage.png)

- Cụ thể, có thể tham khảo tại file: [coverage.html](./docs/coverage/coverage.html)

### Các vấn đề khác: 
- Đã thực hiện build và deploy service Demo lên môi trường server cá nhân (`VPS của Google Cloud - Linux (1 CPU, 4Gb RAM)`)
- Tích hợp CI với Gitlab CI để hỗ trợ các job `unit test`, `lint code` và `build`. Đảm bảo code luôn được clean, không fail
business logic và compile service fail. 
    + ![CI Integration](./docs/images/ci_integration.png)

- Đang implement thêm caching với `Redis` để giảm thiểu các lần chạy thuật toán tìm các set of seats phù hợp, khi không có
sự thay đổi các ghế (picked, blocked, ...) trong rạp. Tuy nhiên vì thời gian không đủ nên phần này đang implement được 70%. 

- Tổ chức source code clean (develop, fix bug or write document on feature branch, after create MR to master), và commit
theo convention chung, dễ nhìn và có thể hỗ trợ gen changlog (_nếu cần_). 
    + ![Gitlab MR](./docs/images/gitlab_merge_request.png)
    + ![Source Tree Commit](./docs/images/source_tree_commit.png)

## Demo <a name="demo"></a>
Now, I had deployed this service on my personal cloud. This support more easy to test and verify all requirement in this
assignment. 

I had run `docker-compose`, and config has been load from environment - which define on [`docker-compose` file](./deployment/docker-compose.yml),
as follows: 

|Config Name| Value  | Note  |
|:---|:---:|---|
| Rows | 4 | |
| Columns | 6 | |
| Min Distance | 1 | |
| Limit Result | 50 | | 
| Port | 8082 | Port gPRC expose |

You can access to address `34.71.225.33:8082` to call and test service with 2 RPC which been define at file proto 
([at hear](./proto/cinema_operator_service.proto)). 

**Example**: 
![Search set of seats available](./docs/images/demo_search_seats.png)


---
###Thanks for reviewing my assignment

- Author: Vinh Huynh Anh
- Email: vinhha.101296@gmail.com
- Phone: 039 604 4353



