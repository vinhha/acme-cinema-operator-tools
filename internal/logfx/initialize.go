package logfx

import (
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"go.uber.org/fx"
)

var Initialize = fx.Invoke(initialize)

func initialize() {
	// Log as JSON instead of the default ASCII formatter.
	productionFlag := viper.GetBool("api.debug")
	if productionFlag {
		log.SetFormatter(&log.JSONFormatter{})
	} else {
		log.SetFormatter(&log.TextFormatter{})
	}

	// Output to stdout instead of the default stderr
	// Can be any io.Writer, see below for File example
	log.SetOutput(os.Stdout)
}
