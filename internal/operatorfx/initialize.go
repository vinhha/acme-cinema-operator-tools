package operatorfx

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"go.uber.org/fx"

	"acme-cinema-operator-tools/pkg/operator"
	"acme-cinema-operator-tools/pkg/utils"
)

var Module = fx.Provide(provideCinemaOperator)

const DefaultLimitResultSet = 50
const MaximumSizeOfCinema = 20

type config struct {
	rows        int
	columns     int
	minDistance int
	limitResult int
}

func provideCinemaOperator() operator.Operator {
	// Step 1: Load config from configuration (file, env, cloud, ...)
	cinemaConfig := loadConfig()

	// Step 2: Check config is valid
	isValid := checkConfigIsValid(cinemaConfig)
	if !isValid {
		logrus.Panic("fail to init cinema operator with config is not valid")
	}
	logrus.WithFields(logrus.Fields{
		"rows":         cinemaConfig.rows,
		"columns":      cinemaConfig.columns,
		"limit_result": cinemaConfig.limitResult,
		"min_distance": cinemaConfig.minDistance,
	}).
		Info("load cinema config and verify successfully")

	return operator.New(cinemaConfig.rows, cinemaConfig.columns, cinemaConfig.minDistance, cinemaConfig.limitResult)
}

func checkConfigIsValid(cinemaConfig config) bool {
	// 1. rows, columns > 0: Check valid rows and columns is large 0
	if cinemaConfig.rows < 0 || cinemaConfig.columns < 0 || cinemaConfig.minDistance < 0 {
		return false
	}

	// 2. min_distance <= rows + columns - 2
	// Because maximum of min_distance is `|rows - 1| + |columns - 1|
	if cinemaConfig.minDistance > cinemaConfig.rows+cinemaConfig.columns-2 {
		logrus.Warnf("min_distance must < |rows - 1| + |columns - 1|")
		return false
	}

	// 3. Check size of cinema (row or column) should not large 20
	if cinemaConfig.columns > MaximumSizeOfCinema || cinemaConfig.rows > MaximumSizeOfCinema {
		logrus.Warnf("size of columns: %d or rows: %d should not large maximum_config: %d",
			cinemaConfig.columns, cinemaConfig.rows, MaximumSizeOfCinema)
		return false
	}

	return true
}

func loadConfig() config {
	rows := viper.GetInt("cinema_config.rows")
	columns := viper.GetInt("cinema_config.columns")
	minDistance := viper.GetInt("cinema_config.min_distance")
	limitResult := viper.GetInt("cinema_config.limit_result")

	utils.NotZeroInt(rows, "config rows is zero, need add into config")
	utils.NotZeroInt(columns, "config columns is zero, need add into config")

	if limitResult == 0 {
		logrus.Infof("config limit result is empty, should set by default with %d", DefaultLimitResultSet)
		limitResult = DefaultLimitResultSet
	}
	return config{
		rows:        rows,
		columns:     columns,
		minDistance: minDistance,
		limitResult: limitResult,
	}
}
