package main

import (
	"go.uber.org/fx"

	"acme-cinema-operator-tools/cmd"
	"acme-cinema-operator-tools/internal/configfx"
	"acme-cinema-operator-tools/internal/logfx"
	"acme-cinema-operator-tools/internal/operatorfx"
)

func main() {
	app := fx.New(
		configfx.Initialize("acme-cinema-operator-tools", "config", "configs"),
		logfx.Initialize,
		operatorfx.Module,

		fx.Invoke(cmd.InitializeService),
	)
	app.Run()
}
