package utils

import "github.com/sirupsen/logrus"

func NotZeroInt(num int, errString string) {
	if num == 0 {
		logrus.Panicf(errString)
	}
}
