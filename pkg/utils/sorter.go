package utils

import (
	"sort"

	"acme-cinema-operator-tools/pkg/models"
)

type lessFunc func(r1, r2 *models.Seat) bool

// multiSorter implements the Sort interface, sorting the rules within.
// Ref: https://golang.org/pkg/sort

type multiSorter struct {
	rules []models.Seat
	less  []lessFunc
}

// Sort sorts the argument slice according to the less functions passed to orderedBy.
func (ms *multiSorter) Sort(rules []models.Seat) {
	ms.rules = rules
	sort.Sort(ms)
}

// orderedBy returns a Sorter that sorts using the less functions, in order.
// Call its Sort method to sort the data.
func OrderedBy(less ...lessFunc) *multiSorter {
	return &multiSorter{
		less: less,
	}
}

func (ms *multiSorter) Len() int {
	return len(ms.rules)
}

func (ms *multiSorter) Swap(i, j int) {
	ms.rules[i], ms.rules[j] = ms.rules[j], ms.rules[i]
}

/* 	Less is part of sort.Interface. It is implemented by looping along the less functions until it finds a comparison
that discriminates between the two items (one is less than the other).
*/
func (ms *multiSorter) Less(i, j int) bool {
	p, q := &ms.rules[i], &ms.rules[j]
	var k int
	for k = 0; k < len(ms.less)-1; k++ {
		less := ms.less[k]
		switch {
		case less(p, q):
			return true
		case less(q, p):
			return false
		}
	}
	return ms.less[k](p, q)
}

func ByRow() func(r1, r2 *models.Seat) bool {
	return func(r1, r2 *models.Seat) bool {
		return r1.Row < r2.Row
	}
}

func ByColumn() func(r1, r2 *models.Seat) bool {
	return func(r1, r2 *models.Seat) bool {
		return r1.Column < r2.Column
	}
}
