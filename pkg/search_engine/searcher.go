package searchengine

import (
	"acme-cinema-operator-tools/pkg/consts"
	"acme-cinema-operator-tools/pkg/models"
	"acme-cinema-operator-tools/pkg/utils"
)

var _finalSetOfSeats [][]models.Seat

type searchEngine struct {
	rows         int
	columns      int
	cinemaMatrix [][]int
}

func SearchSetOfSeats(n, rows, columns int, cinemaMatrix [][]int) [][]models.Seat {
	// Step 0: Init search engine:
	s := searchEngine{
		rows:         rows,
		columns:      columns,
		cinemaMatrix: cinemaMatrix,
	}

	_finalSetOfSeats = make([][]models.Seat, 0)
	result := make([]models.Seat, 0)

	// Step 1: Call to find set of seat
	s.findSetOfSeat(n, result)

	// Step 2: Return result
	return _finalSetOfSeats
}

/*
	Func: Find all set of seats available are based on n (number of seats)
*/
func (s *searchEngine) findSetOfSeat(n int, result []models.Seat) {
	if n == 0 {
		// Copy result to temp result, make sure current set of result not be change
		var temp = make([]models.Seat, len(result))
		copy(temp, result)

		// Should sort all seat in set result increase from left to right, down to up
		utils.OrderedBy(utils.ByRow(), utils.ByColumn()).Sort(temp)

		// Check is exist in _finalSetOfSeats
		if !isExistSetInFinalResult(temp) {
			_finalSetOfSeats = append(_finalSetOfSeats, temp)
		}
		return
	}

	for row := 0; row < s.rows; row++ {
		for column := 0; column < s.columns; column++ {
			if !isCanChoose(s.cinemaMatrix, row, column, result) {
				continue
			}
			result = append(result, models.Seat{
				Row:    row,
				Column: column,
			})
			s.findSetOfSeat(n-1, result)    //nolint:gomnd
			result = result[:len(result)-1] //nolint:gomnd
		}
	}
}

/*
	Func: Check current set is exist in _finalSetOfSeats, if not exist, append result to _finalSetOfSeats
*/
func isExistSetInFinalResult(curResult []models.Seat) bool {
	if len(_finalSetOfSeats) == 0 {
		return false
	}
	var isExist = false
	for _, set := range _finalSetOfSeats {
		for idx := range set {
			if set[idx] != curResult[idx] {
				isExist = false
				break
			}
			isExist = true
		}
		if isExist {
			return true
		}
	}
	return isExist
}

/*
	Func: Check element at position(row, column) is can choose. If element pass all rule:
	+ Value of element is SeatAvailable (0)
	+ Element is not exist in result
	+ Check len(result):
		+ If len(result) == 0 -> Element can choose
		+ If len(result) != 0 -> Should check current element is relate with element in result (next to or below)
*/
func isCanChoose(cinemaMatrix [][]int, row int, column int, result []models.Seat) bool {
	// Check element is not available
	if !isElementAvailable(cinemaMatrix, row, column) {
		return false
	}

	// Check element is exist in result
	for _, seat := range result {
		if row == seat.Row && column == seat.Column {
			return false
		}
	}

	// Check If len of result is empty, element can choose
	if len(result) == 0 {
		return true
	}

	// Check element is right of result
	for _, seat := range result {
		if isRightOfCurrentEle(row, column, seat) || isBelowOfCurrentEle(row, column, seat) {
			return true
		}
	}
	return false
}

/*
	Func: Check one element is available base on value of element at position (row, column)
	+ If matrix[row][column] == SeatAvailable -> return true
	+ If matrix[row][column] != SeatAvailable (Picked, Blocked, ...) -> return false
*/
func isElementAvailable(cinemaMatrix [][]int, row, column int) bool {
	return cinemaMatrix[row][column] == consts.SeatAvailable
}

/*
	Func: Check current element is right of one element in result
*/
func isRightOfCurrentEle(row, column int, seat models.Seat) bool {
	return column-seat.Column == 1 && row == seat.Row
}

/*
	Func: Check current element is right of one element in result
*/
func isBelowOfCurrentEle(row, column int, seat models.Seat) bool {
	return column == seat.Column && row-seat.Row == 1
}

/*
	Func: Check current element is upper of one element in result
*/
func isUpperCurrentEle(row, column int, seat models.Seat) bool {
	return column == seat.Column && seat.Row-row == 1
}

/*
	Func: Check current element is left of one element in result
*/
func isLeftOfCurrentEle(row, column int, seat models.Seat) bool {
	return seat.Column-column == 1 && row == seat.Row
}
