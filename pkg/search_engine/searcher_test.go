package searchengine

import (
	"testing"

	"github.com/sebdah/goldie/v2"

	"acme-cinema-operator-tools/pkg/models"
)

func Test_searchEngine_findSetOfSeat(t *testing.T) {
	type searchEngineParam struct {
		rows         int
		columns      int
		cinemaMatrix [][]int
	}
	tests := []struct {
		name   string
		input  searchEngineParam
		n      int
		result []models.Seat
	}{
/*		{
			name: "Test case 1 - Set of seats is 1",
			input: searchEngineParam{
				rows:    4,
				columns: 5,
				cinemaMatrix: [][]int{
					{-1, 1, -1, -1, 0},
					{0, -1, -1, 1, -1},
					{0, 0, -1, -1, 0},
					{0, -1, 1, -1, 0},
				},
			},
			n: 1,
		},
		{
			name: "Test case 2 - Set of seats is 2",
			input: searchEngineParam{
				rows:    4,
				columns: 5,
				cinemaMatrix: [][]int{
					{-1, 1, -1, -1, 0},
					{0, -1, -1, 1, -1},
					{0, 0, -1, -1, 0},
					{0, -1, 1, -1, 0},
				},
			},
			n: 2,
		},*/
		{
			name: "Test case 3 - Set of seats is 3",
			input: searchEngineParam{
				rows:    4,
				columns: 5,
				cinemaMatrix: [][]int{
					{-1, 1, -1, -1, 0},
					{0, -1, -1, 1, -1},
					{0, 0, -1, -1, 0},
					{0, -1, 1, -1, 0},
				},
			},
			n: 3,
		},
		//{
		//	name: "Test case 4 - Set of seats is 4",
		//	input: searchEngineParam{
		//		rows:    4,
		//		columns: 5,
		//		cinemaMatrix: [][]int{
		//			{-1, 1, -1, -1, 0},
		//			{0, -1, -1, 1, -1},
		//			{0, 0, -1, -1, 0},
		//			{0, -1, 1, -1, 0},
		//		},
		//	},
		//	n: 4,
		//},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			setOfSeats := SearchSetOfSeats(tt.n, tt.input.rows, tt.input.columns, tt.input.cinemaMatrix)
			goldie.New(t).AssertJson(t, t.Name(), setOfSeats)
		})
	}
}
