package searchengine

import "acme-cinema-operator-tools/pkg/models"

var _isSetAvailable bool

func CheckSetIsAvailable(rows, columns int, cinemaMatrix [][]int, setOfSeats []models.Seat) bool {
	// Step 0: Init search engine:
	s := searchEngine{
		rows:         rows,
		columns:      columns,
		cinemaMatrix: cinemaMatrix,
	}

	// Step 1: Call to search set is available?
	_isSetAvailable = false
	result := make([]models.Seat, 0)
	s.searchSetIsAvailable(setOfSeats, result)

	// Return result
	return _isSetAvailable
}

func (s *searchEngine) searchSetIsAvailable(input []models.Seat, result []models.Seat) {
	if len(input) == 0 {
		_isSetAvailable = true
		return
	}

	var temp = make([]models.Seat, len(input))
	copy(temp, input)
	for _, seat := range temp {
		if !s.checkSeatIsAvailable(seat, result) {
			_isSetAvailable = false
			return
		}
		result = append(result, seat)
		input = input[1:]
		s.searchSetIsAvailable(input, result)
	}
}

func (s *searchEngine) checkSeatIsAvailable(curSeat models.Seat, result []models.Seat) bool {
	// Check current seat is available
	if !isElementAvailable(s.cinemaMatrix, curSeat.Row, curSeat.Column) {
		return false
	}
	// Check seat is next to other seat
	for _, seat := range result {
		if curSeat.Row == seat.Row && curSeat.Column == seat.Column {
			return false
		}
	}

	// Check If len of result is empty, element can choose
	if len(result) == 0 {
		return true
	}

	// Check element is right of result
	for _, seat := range result {
		if isRightOfCurrentEle(seat.Row, seat.Column, curSeat) ||
			isBelowOfCurrentEle(seat.Row, seat.Column, curSeat) ||
			isUpperCurrentEle(seat.Row, seat.Column, curSeat) ||
			isLeftOfCurrentEle(seat.Row, seat.Column, curSeat) {
			return true
		}
	}
	return false
}
