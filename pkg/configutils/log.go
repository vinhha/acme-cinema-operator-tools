package configutils

import (
	"fmt"
	"log"
)

var (
	LocalConfigLogPrefix = "[LOCAL CONFIG]"
	WarningLevel         = "[WARNING]"
	ErrorLevel           = "[ERROR]"
	InfoLevel            = "[INFO]"
)

func logWithLevel(level, msg string) {
	log.Println(fmt.Sprintf(" %s %s", level, msg))
}

func logWarn(msg string) {
	logWithLevel(WarningLevel, msg)
}

func logInfo(msg string) {
	logWithLevel(InfoLevel, msg)
}

func logError(msg string) {
	logWithLevel(ErrorLevel, msg)
}
