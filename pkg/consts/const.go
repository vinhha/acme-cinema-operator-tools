package consts

const (
	SeatBlocked   = -1
	SeatAvailable = 0
	SeatPicked    = 1
)
