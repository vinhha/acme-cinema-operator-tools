package errors

import "fmt"

var (
	ErrReqOfSeatNotAvailable = fmt.Errorf("REQ_SET_OF_SEAT_NOT_AVAILABLE")
	ErrNotEnoughSeat         = fmt.Errorf("NOT_ENOUGH_SEAT")
	ErrResultIsEmpty         = fmt.Errorf("RESULT_IS_EMPTY")
	ErrSizeOfSetIsEmpty      = fmt.Errorf("INVALID_PARAM_SIZE_OF_SET")
)
