package handler

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/sebdah/goldie/v2"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"

	"acme-cinema-operator-tools/api/cinema_operator"
	"acme-cinema-operator-tools/pkg/errors"
	"acme-cinema-operator-tools/pkg/models"

	mockoperator "acme-cinema-operator-tools/pkg/operator/gomock"
)

type handlerSuite struct {
	suite.Suite
	goMockCtrl *gomock.Controller

	operatorHandler *mockoperator.MockOperator
	handler         cinema_operator.CinemaOperatorHandlerServer
}

func TestRepoSuite(t *testing.T) {
	suite.Run(t, new(handlerSuite))
}

func (s *handlerSuite) SetupTest() {
	s.goMockCtrl = gomock.NewController(s.T())
	s.operatorHandler = mockoperator.NewMockOperator(s.goMockCtrl)
	s.handler = NewHandler(s.operatorHandler)
}

func (s *handlerSuite) TearDownTest() {
	s.goMockCtrl.Finish()
}

// ------------------------------------------------------------- //
// TEST Search Set of Seats
// ------------------------------------------------------------- //

/*
	Test case 1: Check number of seats from input is 0
	Expect: Should return result
*/
func (s *handlerSuite) TestCase1_HandleSearchSeatWithSizeIsZero() {
	mockRequest := mockQuerySeatsWithSizeZero()
	response, err := s.handler.SearchSeats(context.Background(), mockRequest)
	assert.Error(s.T(), err, errors.ErrSizeOfSetIsEmpty.Error())
	assert.Nil(s.T(), response)
}

/*
	Test case 2: Validate input ok, mock operator biz return 3 set is available
	Expect: Exist 3 set result, error is nil
*/
func (s *handlerSuite) TestCase2_HandleSearchSeatSuccess() {
	mockRequest := mockQuerySeatsAvailable()
	s.operatorHandler.EXPECT().
		QuerySeatsAvailable(3).
		Return([][]models.Seat{
			{{1, 2}, {1, 3}},
			{{1, 2}, {2, 3}},
			{{4, 2}, {3, 3}},
		}, nil)

	response, err := s.handler.SearchSeats(context.Background(), mockRequest)
	assert.NoError(s.T(), err)
	goldie.New(s.T()).AssertJson(s.T(), s.T().Name(), response)
}

/*
	Test case 3: Mock operator return error, because total seats available is empty
	Expect: Result is nil, return error
*/
func (s *handlerSuite) TestCase3_HandleSearchSeatFail() {
	mockRequest := mockQuerySeatsAvailable()
	s.operatorHandler.EXPECT().
		QuerySeatsAvailable(3).
		Return(nil, errors.ErrNotEnoughSeat)

	response, err := s.handler.SearchSeats(context.Background(), mockRequest)
	assert.Error(s.T(), err, errors.ErrNotEnoughSeat.Error())
	assert.Nil(s.T(), response)
}

func mockQuerySeatsWithSizeZero() *cinema_operator.SearchSeatsRequest {
	return &cinema_operator.SearchSeatsRequest{
		NumberSeats: 0,
	}
}

func mockQuerySeatsAvailable() *cinema_operator.SearchSeatsRequest {
	return &cinema_operator.SearchSeatsRequest{
		NumberSeats: 3,
	}
}

// ------------------------------------------------------------- //
// TEST Reserve a set of seat
// ------------------------------------------------------------- //

/*
	Test case 4: Check number of seats from input is 0
	Expect: Should return result
*/
func (s *handlerSuite) TestCase4_HandleReserveSeatsWithSizeIsZero() {
	mockRequest := mockReserveSeatsWithSizeZero()
	response, err := s.handler.ReserveSeats(context.Background(), mockRequest)
	assert.Error(s.T(), err, errors.ErrSizeOfSetIsEmpty.Error())
	assert.Nil(s.T(), response)
}

/*
	Test case 5: Validate input ok, mock operator biz return set of seats is available.
	Expect: Return error is nil
*/
func (s *handlerSuite) TestCase5_HandleReserveSeatsSuccess() {
	mockRequest := mockReserveSeatsSuccess()
	s.operatorHandler.EXPECT().
		ReserveSeats([]models.Seat{
			{0, 0}, {1, 1},
		}).Return(nil)

	response, err := s.handler.ReserveSeats(context.Background(), mockRequest)
	assert.NoError(s.T(), err)
	goldie.New(s.T()).AssertJson(s.T(), s.T().Name(), response)
}

/*
	Test case 6: Mock operator return error, because total seats available is empty
	Expect: Result is nil, return error
*/
func (s *handlerSuite) TestCase6_HandleReserveSeatsFail() {
	mockRequest := mockReserveSeatsSuccess()
	s.operatorHandler.EXPECT().
		ReserveSeats([]models.Seat{
			{0, 0}, {1, 1},
		}).Return(errors.ErrNotEnoughSeat)

	response, err := s.handler.ReserveSeats(context.Background(), mockRequest)
	assert.Error(s.T(), err, errors.ErrNotEnoughSeat.Error())
	assert.Nil(s.T(), response)
}

func mockReserveSeatsSuccess() *cinema_operator.ReserveSeatsRequest {
	return &cinema_operator.ReserveSeatsRequest{
		Seat: []*cinema_operator.Seat{
			{
				X: 0,
				Y: 0,
			},
			{
				X: 1,
				Y: 1,
			},
		},
	}
}

func mockReserveSeatsWithSizeZero() *cinema_operator.ReserveSeatsRequest {
	return &cinema_operator.ReserveSeatsRequest{
		Seat: []*cinema_operator.Seat{},
	}
}
