package handler

import (
	"context"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cast"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"acme-cinema-operator-tools/api/cinema_operator"
	"acme-cinema-operator-tools/pkg/errors"
	"acme-cinema-operator-tools/pkg/models"
)

func (h *handler) SearchSeats(ctx context.Context,
	req *cinema_operator.SearchSeatsRequest) (res *cinema_operator.SearchSeatsResponse, err error) {
	logrus.WithFields(logrus.Fields{"set_seats": req.GetNumberSeats()}).
		Info("[Handler] begin find set of seats available in handler")

	numberOfSeats := cast.ToInt(req.GetNumberSeats())

	if numberOfSeats == 0 {
		err = status.Error(codes.Unknown, errors.ErrSizeOfSetIsEmpty.Error())
		return
	}

	result, err := h.cinemaOperator.QuerySeatsAvailable(numberOfSeats)
	if err != nil {
		err = status.Error(codes.Unknown, err.Error())
		return
	}

	resultSetOfSets := h.transformSearchSeatsResponse(result)
	logrus.WithFields(logrus.Fields{
		"set_seats":   req.GetNumberSeats(),
		"size_result": len(resultSetOfSets),
		"result":      resultSetOfSets,
	}).Info("[Handler] Get all set of seats available successfully")

	res = &cinema_operator.SearchSeatsResponse{
		SetSeats: resultSetOfSets,
		Message:  "Query Success",
	}
	return
}

func (h *handler) transformSearchSeatsResponse(result [][]models.Seat) []*cinema_operator.SetOfSeat {
	logrus.WithFields(logrus.Fields{"len_result": len(result)}).
		Info("[Handler] begin transform result set's of seat to dto of api")
	var arrSetOfSeat = make([]*cinema_operator.SetOfSeat, 0, len(result))
	for _, set := range result {
		var setOfSeats = make([]*cinema_operator.Seat, 0, len(set))
		for _, seat := range set {
			setOfSeats = append(setOfSeats, &cinema_operator.Seat{
				X: cast.ToInt32(seat.Row),
				Y: cast.ToInt32(seat.Column),
			})
		}
		arrSetOfSeat = append(arrSetOfSeat, &cinema_operator.SetOfSeat{Seat: setOfSeats})
	}
	return arrSetOfSeat
}
