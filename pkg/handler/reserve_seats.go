package handler

import (
	"context"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cast"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"acme-cinema-operator-tools/api/cinema_operator"
	"acme-cinema-operator-tools/pkg/errors"
	"acme-cinema-operator-tools/pkg/models"
)

func (h *handler) ReserveSeats(ctx context.Context,
	req *cinema_operator.ReserveSeatsRequest) (res *cinema_operator.ReserveSeatsResponse, err error) {
	logrus.WithFields(logrus.Fields{
		"size_of_set_request": len(req.GetSeat()),
		"set_of_seats":        req.GetSeat()}).
		Info("[Handler] begin reserve seats for user")

	// Check len of request
	if len(req.GetSeat()) == 0 {
		return nil, status.Error(codes.InvalidArgument, errors.ErrSizeOfSetIsEmpty.Error())
	}

	// Call reserve set of seats:
	reserveSeatsReq := h.transformReserveSeats(req)
	err = h.cinemaOperator.ReserveSeats(reserveSeatsReq)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"size_of_set_request": len(req.GetSeat()),
			"error":               err,
		}).Error("[Handler] Fail to call reserve set of seats for User")
		return nil, status.Error(codes.Unknown, err.Error())
	}

	// Return result
	return &cinema_operator.ReserveSeatsResponse{
		Message: "Reserve Success",
	}, nil
}

func (h *handler) transformReserveSeats(req *cinema_operator.ReserveSeatsRequest) []models.Seat {
	var setOfSeats = make([]models.Seat, 0, len(req.GetSeat()))
	for _, seat := range req.GetSeat() {
		setOfSeats = append(setOfSeats, models.Seat{
			Row:    cast.ToInt(seat.X),
			Column: cast.ToInt(seat.Y),
		})
	}
	logrus.WithFields(logrus.Fields{
		"set_of_seats": setOfSeats,
	}).Infof("[Handler] transform reserve seat want to pick success")
	return setOfSeats
}
