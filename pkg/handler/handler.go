package handler

import (
	"acme-cinema-operator-tools/api/cinema_operator"
	"acme-cinema-operator-tools/pkg/operator"
)

type handler struct {
	cinemaOperator operator.Operator
}

func NewHandler(cinemaOperator operator.Operator) cinema_operator.CinemaOperatorHandlerServer {
	return &handler{
		cinemaOperator: cinemaOperator,
	}
}
