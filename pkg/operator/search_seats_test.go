package operator

import (
	"testing"

	"github.com/sebdah/goldie/v2"
	"github.com/stretchr/testify/assert"
)

const DefaultLimitResultSet = 20

func Test_operator_SearchSeats(t *testing.T) {
	tests := []struct {
		name               string
		rows               int
		columns            int
		minDistance        int
		totalSeatAvailable int
		cinemaMatrix       [][]int
		n                  int
		wantErr            bool
	}{
		{
			name:        "Test case 1 - Size of set's request is larger total seat available",
			rows:        4,
			columns:     5,
			minDistance: 1,
			cinemaMatrix: [][]int{
				{0, 1, 0, 0, 0},
				{0, 0, 0, 1, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 1, 0, 0},
			},
			n:       20,
			wantErr: true,
		},
		{
			name:        "Test case 2 - Search engine return all set is empty",
			rows:        4,
			columns:     5,
			minDistance: 2,
			cinemaMatrix: [][]int{
				{0, 1, 0, 0, 0},
				{0, 0, 0, 1, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 1, 0, 0},
			},
			n:       3,
			wantErr: true,
		},
		{
			name:        "Test case 3 - Search success, return all set of seat - sizeOfSeat = 2",
			rows:        4,
			columns:     5,
			minDistance: 1,
			cinemaMatrix: [][]int{
				{0, 1, 0, 0, 0},
				{0, 0, 0, 1, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 1, 0, 0},
			},
			n:       3,
			wantErr: false,
		},
		{
			name:        "Test case 4 - Search success, return all set of seat - sizeOfSeat = 2, minDistance = 2",
			rows:        4,
			columns:     5,
			minDistance: 2,
			cinemaMatrix: [][]int{
				{0, 1, 0, 0, 0},
				{0, 0, 0, 1, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 1, 0, 0},
			},
			n:       2,
			wantErr: false,
		},
		{
			name:        "Test case 5 - Search success, larger 50 result, limit by config",
			rows:        4,
			columns:     6,
			minDistance: 1,
			cinemaMatrix: [][]int{
				{0, 0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0, 0},
			},
			n:       2,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := &operator{
				rows:               tt.rows,
				columns:            tt.columns,
				minDistance:        tt.minDistance,
				limitResultConfig:  DefaultLimitResultSet,
				totalSeatAvailable: tt.totalSeatAvailable,
				cinemaMatrix:       tt.cinemaMatrix,
			}
			o.markSeatShouldBlocked()
			got, err := o.QuerySeatsAvailable(tt.n)
			if tt.wantErr {
				assert.NotNil(t, err)
			} else {
				assert.NoError(t, err)
				goldie.New(t).AssertJson(t, t.Name(), got)
			}
		})
	}
}
