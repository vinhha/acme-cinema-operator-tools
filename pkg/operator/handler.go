package operator

import (
	"github.com/sirupsen/logrus"

	"acme-cinema-operator-tools/pkg/consts"
	"acme-cinema-operator-tools/pkg/models"
)

//go:generate mockgen -source=handler.go -destination=./gomock/handler.go
type Operator interface {
	QuerySeatsAvailable(n int) ([][]models.Seat, error)
	ReserveSeats([]models.Seat) error
}

type operator struct {
	rows               int
	columns            int
	minDistance        int
	limitResultConfig  int
	totalSeatAvailable int
	cinemaMatrix       [][]int
}

func New(rows, columns, minDistance, limitConfig int) Operator {
	cinemaMatrix := initDefaultMatrix(rows, columns)
	return &operator{
		rows:               rows,
		columns:            columns,
		minDistance:        minDistance,
		cinemaMatrix:       cinemaMatrix,
		limitResultConfig:  limitConfig,
		totalSeatAvailable: rows * columns,
	}
}

// Func: Help init matrix with all position has default value = 0 (meaning Seat is Available)
func initDefaultMatrix(rows int, columns int) [][]int {
	logrus.Infof("[Biz] Begin init default matrix with value = 0")
	cinemaMatrix := make([][]int, rows)
	for i := range cinemaMatrix {
		cinemaMatrix[i] = make([]int, columns)
	}
	return cinemaMatrix
}

/*
	Func: Help to run mark all position on matrix which around 1 seat was picked with a distance equal min_distance
*/
func (o *operator) markSeatShouldBlocked() {
	logrus.Infof("[Biz] Begin mark all position should block by social distance")
	// Step 1: 	Scan matrix to check seat was picked (A[i][j]),
	for i := 0; i < o.rows; i++ {
		for j := 0; j < o.columns; j++ {
			if o.cinemaMatrix[i][j] != consts.SeatPicked {
				continue
			}

			for index := 1; index <= o.minDistance; index++ {
				// Check previous element is exist:
				previousEleIdx := j - index
				if previousEleIdx >= 0 && o.cinemaMatrix[i][previousEleIdx] != consts.SeatPicked {
					o.cinemaMatrix[i][previousEleIdx] = consts.SeatBlocked
				}

				// Check upper element is exist:
				upperEleIdx := i - index
				if upperEleIdx >= 0 && o.cinemaMatrix[upperEleIdx][j] != consts.SeatPicked {
					o.cinemaMatrix[upperEleIdx][j] = consts.SeatBlocked
				}

				// Check next element is exist:
				nextEleIdx := j + index
				if nextEleIdx < o.columns && o.cinemaMatrix[i][nextEleIdx] != consts.SeatPicked {
					o.cinemaMatrix[i][nextEleIdx] = consts.SeatBlocked
				}

				belowEleIdx := i + index
				// Check below element is exist:
				if belowEleIdx < o.rows && o.cinemaMatrix[belowEleIdx][j] != consts.SeatPicked {
					o.cinemaMatrix[belowEleIdx][j] = consts.SeatBlocked
				}
			}
		}
	}

	// Step 2: Rerun count seat available in matrix
	o.countSeatIsAvailable()
}

/*
	Func: Func help to count all seat is available in cinema matrix
	+ Seat is marked is Available (=0)
*/
func (o *operator) countSeatIsAvailable() {
	o.totalSeatAvailable = 0
	for i := 0; i < o.rows; i++ {
		for j := 0; j < o.columns; j++ {
			if o.cinemaMatrix[i][j] == consts.SeatAvailable {
				o.totalSeatAvailable++
			}
		}
	}
	logrus.Infof("[Biz] Count seats available successfully with total %d seats", o.totalSeatAvailable)
}
