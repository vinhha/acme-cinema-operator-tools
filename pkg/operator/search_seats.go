package operator

import (
	log "github.com/sirupsen/logrus"

	"acme-cinema-operator-tools/pkg/errors"
	"acme-cinema-operator-tools/pkg/models"
	searchengine "acme-cinema-operator-tools/pkg/search_engine"
)

func (o *operator) QuerySeatsAvailable(n int) ([][]models.Seat, error) {
	// Validate input before search
	if n > o.totalSeatAvailable {
		log.WithFields(log.Fields{"set_of_seat": n}).
			Error("[Biz] total seat available not enough, fail to search set of seats")
		return nil, errors.ErrNotEnoughSeat
	}

	// Call Search Engine to search all set of seats available
	resultSetOfSeats := searchengine.SearchSetOfSeats(n, o.rows, o.columns, o.cinemaMatrix)
	if len(resultSetOfSeats) == 0 {
		log.WithFields(log.Fields{"set_of_seat": n}).
			Error("[Biz] result when search set of seats is empty.")
		return nil, errors.ErrResultIsEmpty
	}

	log.WithFields(log.Fields{"set_of_seat": n, "len_result": len(resultSetOfSeats)}).
		Infof("[Biz] get all set of seats available with set size %d seat", n)

	// Check limit of result:
	if len(resultSetOfSeats) > o.limitResultConfig {
		log.WithFields(log.Fields{"set_of_seat": n, "len_result": len(resultSetOfSeats)}).
			Infof("[Biz] total result found set of seats is larger config limit,"+
				" should get top %d result", o.limitResultConfig)
		resultSetOfSeats = resultSetOfSeats[:o.limitResultConfig]
	}

	// Return all set of seats was found
	return resultSetOfSeats, nil
}
