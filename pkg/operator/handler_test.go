package operator

import (
	"strings"
	"testing"

	"github.com/magiconair/properties/assert"
	"github.com/sebdah/goldie/v2"
	"github.com/spf13/cast"
)

func Test_operator_markSeatShouldBlocked(t *testing.T) {
	tests := []struct {
		name               string
		rows               int
		columns            int
		minDistance        int
		totalSeatAvailable int
		cinemaMatrix       [][]int
	}{
		{
			name:               "Test case 1 - minDistance = 1",
			rows:               4,
			columns:            5,
			minDistance:        1,
			totalSeatAvailable: 7,
			cinemaMatrix: [][]int{
				{0, 1, 0, 0, 0},
				{0, 0, 0, 1, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 1, 0, 0},
			},
		},
		{
			name:               "Test case 2 - minDistance = 2",
			rows:               4,
			columns:            5,
			minDistance:        2,
			totalSeatAvailable: 4,
			cinemaMatrix: [][]int{
				{0, 1, 0, 0, 0},
				{0, 0, 0, 1, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 1, 0, 0},
			},
		},
		{
			name:               "Test case 3 - minDistance = 3",
			rows:               4,
			columns:            5,
			minDistance:        3,
			totalSeatAvailable: 2,
			cinemaMatrix: [][]int{
				{0, 1, 0, 0, 0},
				{0, 0, 0, 1, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 1, 0, 0},
			},
		},
		{
			name:               "Test case 4 - minDistance = 4",
			rows:               4,
			columns:            5,
			minDistance:        4,
			totalSeatAvailable: 2,
			cinemaMatrix: [][]int{
				{0, 1, 0, 0, 0},
				{0, 0, 0, 1, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 1, 0, 0},
			},
		},
		{
			name:               "Test case 5 - minDistance = 2",
			rows:               4,
			columns:            5,
			minDistance:        2,
			totalSeatAvailable: 20,
			cinemaMatrix: [][]int{
				{0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := &operator{
				rows:               tt.rows,
				columns:            tt.columns,
				minDistance:        tt.minDistance,
				totalSeatAvailable: tt.totalSeatAvailable,
				cinemaMatrix:       tt.cinemaMatrix,
			}
			o.markSeatShouldBlocked()
			assert.Equal(t, tt.totalSeatAvailable, o.totalSeatAvailable)
			var str strings.Builder
			for i := range o.cinemaMatrix {
				str.WriteString("[")
				for j := range o.cinemaMatrix[i] {
					str.WriteString(cast.ToString(o.cinemaMatrix[i][j]))
					str.WriteString(", ")
				}
				str.WriteString("]")
				str.WriteString("\n")
			}
			goldie.New(t).Assert(t, t.Name(), []byte(str.String()))
		})
	}
}
