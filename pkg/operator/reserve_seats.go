package operator

import (
	"github.com/sirupsen/logrus"

	"acme-cinema-operator-tools/pkg/consts"
	"acme-cinema-operator-tools/pkg/errors"
	"acme-cinema-operator-tools/pkg/models"
	searchengine "acme-cinema-operator-tools/pkg/search_engine"
	"acme-cinema-operator-tools/pkg/utils"
)

func (o *operator) ReserveSeats(reqSetOfSeats []models.Seat) error {
	n := len(reqSetOfSeats)
	// Check len of set of seats are requesting":
	if n > o.totalSeatAvailable {
		logrus.WithFields(logrus.Fields{"size": n, "total_available": o.totalSeatAvailable}).
			Error("[Biz] total seat available in this cinema is not enough for requirement")
		return errors.ErrNotEnoughSeat
	}

	// Check reqSetOfSeats is exist in setOfSeatsAvailable
	utils.OrderedBy(utils.ByRow(), utils.ByColumn()).Sort(reqSetOfSeats)
	isExistSetAvailable := searchengine.CheckSetIsAvailable(o.rows, o.columns, o.cinemaMatrix, reqSetOfSeats)
	if !isExistSetAvailable {
		logrus.WithFields(logrus.Fields{"size": n}).Errorf("[Biz] not exist set conforms with requirement")
		return errors.ErrReqOfSeatNotAvailable
	}

	//	Mark all position is picked
	for _, seat := range reqSetOfSeats {
		o.cinemaMatrix[seat.Row][seat.Column] = consts.SeatPicked
	}
	o.markSeatShouldBlocked()
	logrus.WithFields(logrus.Fields{"size": n}).
		Info("[Biz] reserve set of seats is successfully")
	return nil
}

/*
	Func: This func comment because this solution not better
*/
//func isExistSetInFinalResult(curResult []models.Seat, finalSetOfSeat [][]models.Seat) bool {
//	var isEqual = true
//	for _, set := range finalSetOfSeat {
//		for idx := range set {
//			if set[idx] != curResult[idx] {
//				isEqual = false
//				break
//			}
//			isEqual = true
//		}
//		if isEqual {
//			return true
//		}
//	}
//	return isEqual
//}
