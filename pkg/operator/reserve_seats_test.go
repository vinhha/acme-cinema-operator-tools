package operator

import (
	"strings"
	"testing"

	"github.com/sebdah/goldie/v2"
	"github.com/spf13/cast"
	"github.com/stretchr/testify/assert"

	"acme-cinema-operator-tools/pkg/errors"
	"acme-cinema-operator-tools/pkg/models"
)

func Test_operator_ReserveSeat(t *testing.T) {
	tests := []struct {
		name               string
		rows               int
		columns            int
		minDistance        int
		totalSeatAvailable int
		cinemaMatrix       [][]int
		reqSetOfSeats      []models.Seat
		wantErr            bool
	}{
		{
			name:               "Test case 1 - Size of set's request is larger total seat available",
			rows:               4,
			columns:            5,
			minDistance:        1,
			totalSeatAvailable: 7,
			cinemaMatrix: [][]int{
				{0, 1, 0, 0, 0},
				{0, 0, 0, 1, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 1, 0, 0},
			},
			reqSetOfSeats: []models.Seat{
				{1, 0}, {2, 0},
				{3, 0}, {4, 0},
				{1, 2}, {2, 2},
				{3, 2}, {0, 2},
			},
			wantErr: true,
		},
		{
			name:               "Test case 2 - Set is not exist in total set of seat found",
			rows:               4,
			columns:            5,
			minDistance:        1,
			totalSeatAvailable: 7,
			cinemaMatrix: [][]int{
				{0, 1, 0, 0, 0},
				{0, 0, 0, 1, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 1, 0, 0},
			},
			reqSetOfSeats: []models.Seat{
				{1, 1}, {2, 0},
			},
			wantErr: true,
		},
		{
			name:               "Test case 3 - Search engine return all set is empty",
			rows:               4,
			columns:            5,
			minDistance:        2,
			totalSeatAvailable: 10,
			cinemaMatrix: [][]int{
				{0, 1, 0, 0, 0},
				{0, 0, 0, 1, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 1, 0, 0},
			},
			reqSetOfSeats: []models.Seat{
				{1, 0}, {2, 0}, {3, 0},
				{0, 0}, {3, 3},
			},
			wantErr: true,
		},
		{
			name:               "Test case 4 - Search is exist in all set found, reserve success with set = 2",
			rows:               4,
			columns:            5,
			minDistance:        1,
			totalSeatAvailable: 7,
			cinemaMatrix: [][]int{
				{0, 1, 0, 0, 0},
				{0, 0, 0, 1, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 1, 0, 0},
			},
			reqSetOfSeats: []models.Seat{
				{2, 0}, {1, 0},
			},
			wantErr: false,
		},
		{
			name:               "Test case 5 - Search is exist in all set found, reserve success with set = 4",
			rows:               4,
			columns:            5,
			minDistance:        1,
			totalSeatAvailable: 7,
			cinemaMatrix: [][]int{
				{0, 1, 0, 0, 0},
				{0, 0, 0, 1, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 1, 0, 0},
			},
			reqSetOfSeats: []models.Seat{
				{1, 0}, {2, 0}, {3, 0}, {2, 1},
			},
			wantErr: false,
		},
		{
			name:               "Test case 6 - Search is exist in all set found, reserve success with set = 3",
			rows:               4,
			columns:            5,
			minDistance:        1,
			totalSeatAvailable: 7,
			cinemaMatrix: [][]int{
				{0, 1, 0, 0, 0},
				{0, 0, 0, 1, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 1, 0, 0},
			},
			reqSetOfSeats: []models.Seat{
				{1, 0}, {2, 1}, {2, 0},
			},
			wantErr: false,
		},
		{
			name:               "Test case 7 - Search is not exist in all set found, reserve success with set = 3",
			rows:               4,
			columns:            5,
			minDistance:        1,
			totalSeatAvailable: 7,
			cinemaMatrix: [][]int{
				{0, 1, 0, 0, 0},
				{0, 0, 0, 1, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 1, 0, 0},
			},
			reqSetOfSeats: []models.Seat{
				{1, 0}, {2, 1}, {3, 4},
			},
			wantErr: true,
		},
		{
			name:               "Test case 8 - Duplicate seat in set of request, reserve success with set = 3",
			rows:               4,
			columns:            5,
			minDistance:        1,
			totalSeatAvailable: 7,
			cinemaMatrix: [][]int{
				{0, 1, 0, 0, 0},
				{0, 0, 0, 1, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 1, 0, 0},
			},
			reqSetOfSeats: []models.Seat{
				{1, 0}, {1, 0}, {3, 4},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := &operator{
				rows:               tt.rows,
				columns:            tt.columns,
				minDistance:        tt.minDistance,
				totalSeatAvailable: tt.totalSeatAvailable,
				cinemaMatrix:       tt.cinemaMatrix,
			}
			o.markSeatShouldBlocked()
			err := o.ReserveSeats(tt.reqSetOfSeats)
			if tt.wantErr {
				assert.Error(t, err, errors.ErrNotEnoughSeat.Error())
			} else {
				assert.NoError(t, err)
				var str strings.Builder
				for i := range o.cinemaMatrix {
					str.WriteString("[")
					for j := range o.cinemaMatrix[i] {
						str.WriteString(cast.ToString(o.cinemaMatrix[i][j]))
						str.WriteString(", ")
					}
					str.WriteString("]")
					str.WriteString("\n")
				}
				goldie.New(t).Assert(t, t.Name(), []byte(str.String()))
			}
		})
	}
}

//func Test_isExistSetInFinalResult(t *testing.T) {
//	tests := []struct {
//		name           string
//		curResult      []models.Seat
//		finalSetOfSeat [][]models.Seat
//		outputExpect   bool
//	}{
//		{
//			name: "Test case 1 - Not exist in Final Result",
//			curResult: []models.Seat{
//				{0, 1}, {1, 0},
//			},
//			finalSetOfSeat: [][]models.Seat{
//				{{0, 0}, {0, 1}},
//				{{1, 1}, {1, 2}},
//				{{0, 1}, {2, 1}},
//			},
//			outputExpect: false,
//		},
//		{
//			name: "Test case 2 - Exist in Final Result",
//			curResult: []models.Seat{
//				{0, 1}, {1, 0},
//			},
//			finalSetOfSeat: [][]models.Seat{
//				{{0, 0}, {0, 1}},
//				{{1, 1}, {1, 2}},
//				{{0, 1}, {1, 0},},
//			},
//			outputExpect: true,
//		},
//	}
//	for _, tt := range tests {
//		t.Run(tt.name, func(t *testing.T) {
//			got := isExistSetInFinalResult(tt.curResult, tt.finalSetOfSeat)
//			assert.Equal(t, tt.outputExpect, got)
//		})
//	}
//}
