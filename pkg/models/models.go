package models

type Seat struct {
	Row    int
	Column int
}
