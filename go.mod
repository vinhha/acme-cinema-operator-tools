module acme-cinema-operator-tools

go 1.14

require (
	github.com/golang/mock v1.3.1
	github.com/golang/protobuf v1.4.2
	github.com/magiconair/properties v1.8.1
	github.com/sebdah/goldie/v2 v2.5.1
	github.com/sirupsen/logrus v1.6.0
	github.com/spf13/cast v1.3.0
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.4.0
	go.uber.org/fx v1.13.1
	google.golang.org/grpc v1.31.0
	google.golang.org/protobuf v1.25.0
)
