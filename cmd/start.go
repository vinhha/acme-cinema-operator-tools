package cmd

import (
	"context"
	"fmt"
	"log"
	"net"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"go.uber.org/fx"
	"google.golang.org/grpc"

	"acme-cinema-operator-tools/api/cinema_operator"
	"acme-cinema-operator-tools/pkg/handler"
	"acme-cinema-operator-tools/pkg/operator"
)

type Param struct {
	fx.In
	CinemaOperator operator.Operator
	LifeCycle      fx.Lifecycle
}

func InitializeService(p Param) {
	port := getPort()
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		log.Fatalf("failed to listen service at port %d with error: %v", port, err)
	}

	gRPCServer := grpc.NewServer()
	handlerService := handler.NewHandler(p.CinemaOperator)
	cinema_operator.RegisterCinemaOperatorHandlerServer(gRPCServer, handlerService)

	p.LifeCycle.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			logrus.Infof("server starts at port: %d", port)
			go func() {
				err := gRPCServer.Serve(lis)
				if err != nil {
					logrus.Errorf("failed to start server: %s", err)
				}
			}()
			return nil
		},
		OnStop: func(ctx context.Context) error {
			logrus.Infof("server stops")
			gRPCServer.GracefulStop()
			return nil
		},
	})
}

func getPort() int {
	port := viper.GetInt("api.port")
	if port == 0 {
		logrus.Panicf("empty port of service, should init config api.port")
	}
	return port
}
